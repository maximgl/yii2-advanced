<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HomeFeature */

$this->title = 'Create Home Feature';
$this->params['breadcrumbs'][] = ['label' => 'Home Features', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-feature-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
