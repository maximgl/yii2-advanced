<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HomeFeatureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Home Features';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-feature-index">


    <p>
        <?= Html::a('Создать Home Feature', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'status',
            'smallImage:image',
            'title',
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
