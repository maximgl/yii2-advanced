<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PricingPlansCard */

$this->title = 'Update Pricing Plans Card: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Pricing Plans Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pricing-plans-card-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
