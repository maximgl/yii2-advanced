<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PricingPlansCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pricing Plans Cards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pricing-plans-card-index">


    <p>
        <?= Html::a('Create Pricing Plans Card', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'status',
            'title',
            'price',
            'specifications',
            //'button_text',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
