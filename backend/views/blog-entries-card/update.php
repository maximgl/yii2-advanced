<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BlogEntriesCard */

$this->title = 'Update Blog Entries Card: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Blog Entries Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="blog-entries-card-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
