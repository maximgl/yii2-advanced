<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BlogEntries */

$this->title = 'Update Blog Entries: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Blog Entries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="blog-entries-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
