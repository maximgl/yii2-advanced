<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BlogEntries */

$this->title = 'Create Blog Entries';
$this->params['breadcrumbs'][] = ['label' => 'Blog Entries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-entries-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
