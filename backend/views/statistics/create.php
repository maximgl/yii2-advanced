<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Statistics */

$this->title = 'Create Statistics';
$this->params['breadcrumbs'][] = ['label' => 'Statistics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="statistics-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
