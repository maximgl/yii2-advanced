<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PricingPlans */

$this->title = 'Create Pricing Plans';
$this->params['breadcrumbs'][] = ['label' => 'Pricing Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pricing-plans-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
