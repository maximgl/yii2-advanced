<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PricingPlans */

$this->title = 'Update Pricing Plans: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Pricing Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pricing-plans-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
