<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PricingPlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pricing Plans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pricing-plans-index">


    <p>
        <?= Html::a('Create Pricing Plans', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'status',
            'title',
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
