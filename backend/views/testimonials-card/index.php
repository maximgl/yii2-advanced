<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TestimonialsCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Testimonials Cards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testimonials-card-index">


    <p>
        <?= Html::a('Create Testimonials Card', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'status',
            'text',
            'name',
            'position',
            'smallImage:image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
