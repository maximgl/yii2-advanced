<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TestimonialsCard */

$this->title = 'Create Testimonials Card';
$this->params['breadcrumbs'][] = ['label' => 'Testimonials Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testimonials-card-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
