<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TestimonialsCard */

$this->title = 'Update Testimonials Card: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Testimonials Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="testimonials-card-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
