<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WorkProcess */

$this->title = 'Update Work Process: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Work Processes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="work-process-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
