<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WorkProcess */

$this->title = 'Create Work Process';
$this->params['breadcrumbs'][] = ['label' => 'Work Processes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-process-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
