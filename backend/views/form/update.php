<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Form */

$this->title = 'Update Form: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="form-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
