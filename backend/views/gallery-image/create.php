<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GalleryImage */

$this->title = 'Create Gallery Image';
$this->params['breadcrumbs'][] = ['label' => 'Gallery Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-image-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
