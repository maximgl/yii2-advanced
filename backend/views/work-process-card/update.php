<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WorkProcessCard */

$this->title = 'Update Work Process Card: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Work Process Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="work-process-card-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
