<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\WorkProcessCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Work Process Cards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-process-card-index">


    <p>
        <?= Html::a('Create Work Process Card', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'status',
            'smallImage:image',
            'title',
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
