<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WorkProcessCard */

$this->title = 'Create Work Process Card';
$this->params['breadcrumbs'][] = ['label' => 'Work Process Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-process-card-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
