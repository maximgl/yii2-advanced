<?php
namespace frontend\controllers;

use common\models\BlogEntries;
use common\models\BlogEntriesCard;
use common\models\Form;
use common\models\GalleryImage;
use common\models\Map;
use common\models\PricingPlans;
use common\models\PricingPlansCard;
use common\models\Statistics;
use common\models\Testimonials;
use common\models\TestimonialsCard;
use common\models\WorkProcess;
use common\models\WorkProcessCard;
use Yii;
use yii\web\Controller;
use common\models\HeaderText;
use common\models\HomeFeature;
use common\models\About;
use yii\web\Request;
use yii\web\UploadedFile;

class BlogController extends Controller{

    public function actionIndex(){
        $header_text = HeaderText::find()->andWhere(['Status'=>1])->one();

        $home_feature = HomeFeature::find()->andWhere(['Status'=>1])->all();

        $about1 = About::find()->andWhere(['Status'=>1])->one();
        $about2 = About::find()->andWhere(['Status'=>2])->one();

        $work_process = WorkProcess::find()->andWhere(['Status'=>1])->one();
        $work_process_card = WorkProcessCard::find()->andWhere(['Status'=>1])->all();

        $testimonials = Testimonials::find()->andWhere(['Status'=>1])->one();
        $testimonials_card = TestimonialsCard::find()->andWhere(['Status'=>1])->all();

        $pricingPlans = PricingPlans::find()->andWhere(['Status'=>1])->one();
        $pricingPlansCard = PricingPlansCard::find()->andWhere(['Status'=>1])->all();

        $statistics = Statistics::find()->andWhere(['Status'=>1])->all();

        $blogEntries = BlogEntries::find()->andWhere(['Status'=>1])->one();
        $blogEntriesCard = BlogEntriesCard::find()->andWhere(['Status'=>1])->all();
        
        return $this ->render('index',
        [
            'header_text' => $header_text,

            'home_feature' => $home_feature,

            'about1' => $about1,
            'about2' => $about2,

            'work_process' => $work_process,
            'work_process_card' => $work_process_card,

            'testimonials' => $testimonials,
            'testimonials_card' => $testimonials_card,

            'pricingPlans' => $pricingPlans,
            'pricingPlansCard' => $pricingPlansCard,

            'statistics' => $statistics,

            'blogEntries' => $blogEntries,
            'blogEntriesCard' => $blogEntriesCard
        ]
        );
    }

    public function actionMap(){
        $map = Map::find()->andWhere(['Status'=>1])->one();
        $galleryImages = GalleryImage::find()->one();
        $model = new Form();
        if ($model->load($this->request->post()) && $model->save()) {
            if($model -> validate()){
                Yii::$app->session->setFlash('success', 'Данные отправленны');
                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file)){
                    $model->file->saveAs('@frontend/web/files/'. $model->file->baseName. "." . $model->file->extension);
                }
                return $this->refresh();
            }
            else{
                Yii::$app->session->setFlash('error', 'Ошибка');
            }
        }

        return $this ->render('map',
        [
            'map' => $map,
            'model' => $model,
            'galleryImages' =>$galleryImages,
        ]);
    }
}