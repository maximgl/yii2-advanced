<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/flex-slider.css',
        'css/font-awesome.css',
        'css/site.css',
        'css/templatemo-softy-pinko.css',
        'fonts/Flaticon.woff',
        'fonts/flexslider-icon.eot',
        'fonts/flexslider-icon.svg',
        'fonts/flexslider-icon.ttf',
        'fonts/flexslider-icon.woff',
        'fonts/FontAwesome.otf',
        'fonts/fontawesome-webfont.eot',
        'fonts/fontawesome-webfont.svg',
        'fonts/fontawesome-webfont.ttf',
        'fonts/fontawesome-webfont.woff',
        'fonts/fontawesome-webfont.woff2',
        'fonts/slick.eot',
        'fonts/slick.svg',
        'fonts/slick.ttf',
        'fonts/slick.woff',
    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/custom.js',
        'js/imgfix.min.js',
        'js/jquery.counterup.min.js',
//        'js/jquery-2.1.0.min.js',
        'js/popper.js',
        'js/scrollreveal.min.js',
        'js/waypoints.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
