<?php

use kartik\social\FacebookPlugin;
use yii\helpers\Html;
use common\models\Map;
use frontend\models\ContactForm;
use yii\widgets\ActiveForm;
use kartik\social\TwitterPlugin;
use kartik\social\VKPlugin;
use common\models\GalleryImage;

/** @var $map Map */
/** @var $galleryImages GalleryImage */
/** @var $model ContactForm */
/** @var $form yii\widgets\ActiveForm */
?>

<section class="section colored" id="contact-us">
    <div class="container">
        <div class="map">
            <?= $map->text ?>
        </div>

        <div class="col-lg-12">
            <div class="center-heading">
                <h2 class="section-title">Галерея</h2>
            </div>
        </div>
        <div class="gallery_wrapper_parent">
            <div class="gallery_wrapper">
                <?php
                $images = $galleryImages->getImages();
                foreach ($images as $img) :
                    ?>
                    <img class="gallery_img" src="<?= $img->getUrl(); ?>" alt="">
                <?php
                endforeach; ?>
            </div>
        </div>
        <!-- ***** Section Title Start ***** -->
        <div class="row">
            <div class="col-lg-12">
                <div class="center-heading">
                    <h2 class="section-title">Напишите нам</h2>
                </div>
            </div>
            <div class="offset-lg-3 col-lg-6">
                <div class="center-text">
                    <p>Если вы хотите оставить свои отзывы, пожелания или жалобы вы смело можете написать нам</p>
                </div>
            </div>
        </div>
        <!-- ***** Section Title End ***** -->

        <div class="row">
            <!-- ***** Contact Text Start ***** -->
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h5 class="margin-bottom-30">Пожелания к отзыву</h5>
                <div class="contact-text">
                    <p>Пожалуйста, указывайте свою действительную электорную почту, для того чтобы мы могли вам ответить.</p>
                    <p>При желании вы можете прикрепить файл формата "pdf",не более 10мб, с примером жалобы или предложения.</p>
                </div>
            </div>
            <!-- ***** Contact Text End ***** -->

            <!-- ***** Contact Form Start ***** -->
            <div class="col-lg-8 col-md-6 col-sm-12">
                <div class="contact-form">
                    <?php
                    $form = ActiveForm::begin(['action' => '/blog/map']) ?>
                    <form id="contact" action="#" method="get">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <fieldset>
                                    <?= $form->field($model, 'name')->textInput(
                                        ['maxlength' => true, 'minlength' => true, 'placeholder' => 'Имя']
                                    )->label(false) ?>
                                </fieldset>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <fieldset>
                                    <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(
                                        false
                                    ) ?>
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <?= $form->field($model, 'message')->textarea(
                                        ['maxlength' => true, 'minlength' => true, 'placeholder' => 'Сообщение']
                                    )->label(false) ?>
                                </fieldset>
                            </div>
                            <?= $form->field($model, 'file')->fileInput()->label(false); ?>
                            <div class="col-lg-12">
                                <fieldset>
                                    <div id="form-submit" class="main-button">
                                        <?= Html::submitButton(
                                            'Отправить сообщение',
                                            ['class' => 'main-button', 'id' => 'form-submit']
                                        ) ?>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="reCaptcha">
                            <?= $form->field($model, 'reCaptcha')->widget(
                                \himiklab\yii2\recaptcha\ReCaptcha2::className(),
                                [
                                    'siteKey' => '6LeypDccAAAAAA_XhJis1D9kkv1iKbRrT1a5-ar9',
                                ]
                            )->label(false) ?>
                        </div>
                    </form>
                    <?php
                    $form = ActiveForm::end() ?>
                </div>
            </div>
            <!-- ***** Contact Form End ***** -->

        </div>
    </div>
</section>