<?php

use yii\db\Migration;

/**
 * Class m210908_140534_create_work_process_card
 */
class m210908_140534_create_work_process_card extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('work_process_card',[
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'image' => $this->string(100),
            'title' => $this->string(10),
            'description' => $this->string( 30)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('work_process_card');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210908_140534_create_work_process_card cannot be reverted.\n";

        return false;
    }
    */
}
