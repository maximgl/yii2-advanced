<?php

use yii\db\Migration;

/**
 * Class m210711_101057_create_about
 */
class m210711_101057_create_about extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('about', [
            'status' => $this->boolean(),
            'id' => $this->primaryKey(),
            'image' => $this->string(100),
            'title' => $this->string(50),
            'description' => $this->string(150)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('about');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210711_101057_create_about cannot be reverted.\n";

        return false;
    }
    */
}
