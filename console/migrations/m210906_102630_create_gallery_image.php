<?php

use yii\db\Migration;

/**
 * Class m210906_102630_create_gallery_image
 */
class m210906_102630_create_gallery_image extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('gallery_image', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('gallery_image');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210906_102630_create_gallery_image cannot be reverted.\n";

        return false;
    }
    */
}
