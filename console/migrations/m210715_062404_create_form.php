<?php

use yii\db\Migration;

/**
 * Class m210715_062404_create_form
 */
class m210715_062404_create_form extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('form', [
            'id' => $this->primaryKey(),
            'name' => $this->string(30)->notNull(),
            'email' => $this->string(30)->notNull(),
            'message' => $this->string(100)->notNull(),
            'files' => $this->string(100)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('form');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210715_062404_create_form cannot be reverted.\n";

        return false;
    }
    */
}
