<?php

use yii\db\Migration;

/**
 * Class m210909_091941_create_testimonials
 */
class m210909_091941_create_testimonials extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('testimonials',[
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'title' => $this->string(55),
            'description' => $this->string(125),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('testimonials');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_091941_create_testimonials cannot be reverted.\n";

        return false;
    }
    */
}
