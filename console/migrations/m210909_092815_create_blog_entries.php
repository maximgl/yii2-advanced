<?php

use yii\db\Migration;

/**
 * Class m210909_092815_create_blog_entries
 */
class m210909_092815_create_blog_entries extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('blog_entries',[
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'title' => $this->string(55),
            'description' => $this->string(125),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('blog_entries');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_092815_create_blog_entries cannot be reverted.\n";

        return false;
    }
    */
}
