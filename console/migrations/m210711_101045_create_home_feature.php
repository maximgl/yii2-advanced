<?php

use yii\db\Migration;

/**
 * Class m210711_101045_create_home_feature
 */
class m210711_101045_create_home_feature extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('home_feature', [
            'status' => $this->boolean(),
            'id' => $this->primaryKey(),
            'image' => $this->string(100),
            'title' => $this->string(30),
            'description' => $this->string(70)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('home_feature');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210711_101045_create_home_feature cannot be reverted.\n";

        return false;
    }
    */
}
