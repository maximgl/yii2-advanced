<?php

use yii\db\Migration;

/**
 * Class m210908_140515_create_work_process
 */
class m210908_140515_create_work_process extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('work_process',[
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'text' => $this->string(55),
            'description' => $this->string(125),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('work_process');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210908_140515_create_work_process cannot be reverted.\n";

        return false;
    }
    */
}
