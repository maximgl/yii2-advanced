<?php

use yii\db\Migration;

/**
 * Class m210909_092105_create_testimonials_card
 */
class m210909_092105_create_testimonials_card extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('testimonials_card',[
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'text' => $this->string(130),
            'name' => $this->string( 20),
            'position' => $this->string(30),
            'image' => $this->string(100)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        ;$this->dropTable('testimonials_card');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_092105_create_testimonials_card cannot be reverted.\n";

        return false;
    }
    */
}
