<?php

use yii\db\Migration;

/**
 * Class m210711_101027_create_header_text
 */
class m210711_101027_create_header_text extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('header_text', [
            'status' => $this->boolean(),
            'id' => $this->primaryKey(),
            'text' => $this->string(55),
            'description' => $this->string(125),
            'button_text' => $this->string(10)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('header_text');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210711_101027_create_header_text cannot be reverted.\n";

        return false;
    }
    */
}
