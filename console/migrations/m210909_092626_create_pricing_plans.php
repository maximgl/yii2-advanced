<?php

use yii\db\Migration;

/**
 * Class m210909_092626_create_pricing_plans
 */
class m210909_092626_create_pricing_plans extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pricing_plans',[
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'title' => $this->string(20),
            'description' => $this->string(120)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pricing_plans');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_092626_create_pricing_plans cannot be reverted.\n";

        return false;
    }
    */
}
