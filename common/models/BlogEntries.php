<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "blog_entries".
 *
 * @property int $id
 * @property int|null $status
 * @property string|null $title
 * @property string|null $description
 */
class BlogEntries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog_entries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['title'], 'string', 'max' => 55],
            [['description'], 'string', 'max' => 125],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status' => 'Статус',
            'title' => 'Заголовок',
            'description' => 'Описание',
        ];
    }
}
