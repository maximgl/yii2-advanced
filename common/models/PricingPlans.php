<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pricing_plans".
 *
 * @property int $id
 * @property int|null $status
 * @property string|null $title
 * @property string|null $description
 */
class PricingPlans extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pricing_plans';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['title'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 120],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status' => 'Статус',
            'title' => 'Заголовок',
            'description' => 'Описание',
        ];
    }
}
