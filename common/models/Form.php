<?php

namespace common\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator2;
use yii\helpers\Url;
use Yii;

/**
 * This is the model class for table "form".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $message
 * @property string|null $files
 */
class Form extends \yii\db\ActiveRecord
{
    public $reCaptcha;
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'message'], 'required'],
            [['name', 'email'], 'string', 'max' => 30],
            [['message', 'files'], 'string', 'max' => 100],
            [['reCaptcha'], ReCaptchaValidator2::className(),
                'secret' => '6LeypDccAAAAANWm8d5XP8UL8ywXWc-_lLzJZu2o',
                'uncheckedMessage' => 'Please confirm that you are not a bot.'],
            [['file'], 'file', 'extensions'=> ['pdf'], 'maxSize' => 10000 * 1000]
        ];
    }

    /**
     * {@inheritdoc}
     */

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'message' => 'Message',
            'fileName' => 'Files',
        ];
    }

    public function getFileName()
    {
        $dir = '/files/';
        return $dir.$this->files();
    }
}
