<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "header_text".
 *
 * @property int|null $status
 * @property int $id
 * @property string|null $text
 * @property string|null $description
 * @property string|null $button_text
 */
class HeaderText extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'header_text';
    }

    function behaviors()
    {
        return [
            'seo' => [
                'class' => 'dvizh\seo\behaviors\SeoFields',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['text'], 'string', 'max' => 55],
            [['description'], 'string', 'max' => 125],
            [['button_text'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status' => 'Статус',
            'id' => 'ID',
            'text' => 'Текст',
            'description' => 'Описание',
            'button_text' => 'Текст кнопки',
        ];
    }
}
