<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "work_process".
 *
 * @property int $id
 * @property int|null $status
 * @property string|null $text
 * @property string|null $description
 */
class WorkProcess extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work_process';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['text'], 'string', 'max' => 55],
            [['description'], 'string', 'max' => 125],
        ];
    }

    function behaviors()
    {
        return [
            'seo' => [
                'class' => 'dvizh\seo\behaviors\SeoFields',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status' => 'Статус',
            'text' => 'Заголовок',
            'description' => 'Описание',
        ];
    }
}
